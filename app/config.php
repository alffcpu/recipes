<?php
$config = [
	//db settings
	'db' => [
		'host' => 'localhost',
		'user' => 'postgres',
		'passowrd' => '',
		'basename' => 'postgres'
	],

	//error reporting level
	'error_level' => E_ALL,

	//Controllers and methods accessing without token
	'public_access' => [
		'UserController' => ['register', 'authenticate']
	],

	//password hash algo
	'password_algo' => PASSWORD_BCRYPT,

	//max size of lists
	'query_limit' => 100,

	//where to upload user files
	'upload_dir' => '/uploads/',
	'upload_dir_rights' => 0777,
	'allowed_extensions' => ['jpg','jpeg','gif','png'],
	'upload_url' => "http://".$_SERVER['HTTP_HOST']
];
