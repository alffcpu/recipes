<?php
namespace Thesuper\Recipes\Core;

/**
 * Class Request
 * @package Thesuper\Recipes\Core
 */
class Request {

	/**
	 * Stores http request data
	 * @var array
	 */
	public static $data = null;

	/**
	 * Stores http request method in lowcase
	 * @var string
	 */
	private static $method = null;

	/**
	 * Indicates if request had json data
	 * @var bool
	 */
	private static $is_json = false;

	/**
	 * Finds out type of request and initializes request data
	 *
	 * @return array|mixed
	 */
	private static function data() {
		if (self::$data === null) {
			self::$method = strtolower($_SERVER['REQUEST_METHOD']);
			$data = $_GET;
			if (in_array(self::$method, ['post', 'put', 'delete', 'options', 'json', 'jsonp'])) {
				$data = $_POST;
				$raw_input = self::raw_input();
				if ( ($json = json_decode($raw_input, TRUE)) !== null ) {
					$data = $json;
					self::$is_json = true;
				}
			}
			//TODO Add something like security filter
			self::$data = $data;
		}
		return self::$data;
	}

	/**
	 * Returns sigle value from request
	 *
	 * @param $key
	 * @param null $default
	 * @return mixed|null
	 */
	public static function value($key, $default = null) {
		if (!self::$data) self::data();
		return isset(self::$data[$key]) ? self::$data[$key] : $default;
	}

	/**
	 * Returns multiple existing values from request as array
	 *
	 * @param $keys
	 * @param null $default
	 * @return array
	 */
	public static function values($keys, $default = null) {
		if (!self::$data) self::data();
		$result = [];
		if (is_array($keys)) {
			foreach ($keys as $key) {
				$result[$key] = isset(self::$data[$key]) ? self::$data[$key] : $default;
			}
		}
		return $result;
	}

	/**
	 * Checks if key set in request
	 *
	 * @param $key
	 * @param null $default
	 * @return bool
	 */
	public static function is_set($key, $default = null) {
		if (!self::$data) self::data();
		return isset(self::$data[$key]);
	}

	/**
	 * Returns request method in lowcase (post/get/json/etc)
	 *
	 * @return string
	 */
	public static function method() {
		return self::$method;
	}

	/**
	 * Gets raw http data provided by webserver
	 *
	 * @return bool|string
	 */
	public static function raw_input() {
		return file_get_contents('php://input', 'r');
	}

	/**
	 * Returns indicator if request was in json format (eg. json data was found)
	 *
	 * @return bool
	 */
	public static function is_json() {
		return self::$is_json;
	}

}