<?php
namespace Thesuper\Recipes\Core;

/**
 * Class Router
 * @package Thesuper\Recipes\Core
 */
class Router {

	/**
	 * Executes base application routing according to $_SERVER['REQUEST_URI']
	 */
	public static function execute() {
		$uri = self::uri();
		if (!empty($uri)) {
			$file_name = strtolower(preg_replace("/[^A-Za-z0-9_\-]/", '', $uri[0]));
			$file_path = Config::get('controllers_path')."{$file_name}.php";
			if (file_exists($file_path)) {
				$class_name = ucfirst($file_name).'Controller';
				$controller_name = "Thesuper\\Recipes\\Controllers\\{$class_name}";
				$method_name = isset($uri[1]) ? strtolower(preg_replace("/[^A-Za-z0-9_]/", '', $uri[1])) : 'index';
				$request_method = Request::method();
				$method_with_request = $request_method ? "{$request_method}_{$method_name}" : null;
				$instance = new $controller_name();

				//controllers with global remap method
				if (method_exists($instance, 'remap')) {
					array_shift($uri);
					$instance->access($class_name, 'remap');
					$instance->remap($uri);
					return;
				//usual methods
				} else if ($method_name && method_exists($instance, $method_name)) {
					$instance->access($class_name, $method_name);
					$instance->{$method_name}();
					return;
				//methods prefixed with REQUEST_METHOD (post_methodname)
				} else if ($method_with_request && method_exists($instance, $method_with_request)) {
					$instance->access($class_name, $method_with_request);
					$instance->{$method_with_request}();
					return;
				}
			}
		}
		Response::error('API method not found');
		Response::set_error_code(404);
	}

	/**
	 * Prepares $_SERVER['REQUEST_URI'] stirnd and explodes it to array
	 *
	 * @return array
	 */
	private static function uri() {
		return explode('/', explode('?', trim($_SERVER['REQUEST_URI'], '/'))[0]);
	}
}