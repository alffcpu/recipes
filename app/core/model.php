<?php
namespace Thesuper\Recipes\Core;

use \PDO;

/**
 * Class Model
 * @package Thesuper\Recipes\Core
 */
class Model {

	/**
	 * Stores current PDO statement
	 * @var
	 */
	protected $statement;

	/**
	 * Query bindings (substitutions inside PDO prepared query)
	 * @var array
	 */
	protected $bindings = [];

	/**
	 * Query WHERE part
	 * @var array
	 */
	protected $where_array = [];

	/**
	 * Query SET part
	 * @var array
	 */
	protected $sets = [];

	/**
	 * Query VALUES part
	 * @var array
	 */
	protected $values = [];
	protected $last_sql = '';


	/**
	 * Returns database table name
	 *
	 * @return null
	 */
	public function tablename() {
		return isset($this->table) ? $this->table : null;
	}

	/**
	 * Fetches row by id
	 *
	 * @param $id
	 * @return mixed
	 */
	public function id($id) {
		return $this->select(['id' => $id]);
	}

	/**
	 * Executes SELECT query with given parameters and returns single row
	 *
	 * @param $where
	 * @param string $end
	 * @param string $select
	 * @return mixed
	 */
	public function select($where, $end = ';', $select = '*') {
		$this->reset_internal_data();
		$this->where($where);
		$this->last_sql = "SELECT {$select} FROM \"{$this->table}\" WHERE ".implode(' AND ', $this->where_array).$end;
		return $result = $this->query($this->last_sql)
			->bind_array()
			->result_row();
	}

	/**
	 * Executes SELECT query with parameters and returns multiple rows
	 *
	 * @param $where
	 * @param string $end
	 * @param string $select
	 * @return mixed
	 */
	public function get_list($where, $end = ';', $select = '*') {
		$this->reset_internal_data();
		$this->where($where);
		$this->last_sql = "SELECT {$select} FROM \"{$this->table}\" WHERE ".implode(' AND ', $this->where_array).$end;
		return $this->query($this->last_sql)
			->bind_array()
			->result();
	}

	/**
	 * Prepares WHERE part of request
	 *
	 * @param array $where
	 */
	protected function where($where = []) {
		$this->where_array = [];
		foreach ($where as $field => $value) {
			$operation = '=';
			if (stristr($field, ' ')) {
				$field_split = explode(' ', $field);
				$field = array_shift($field_split);
				$operation = implode(' ', $field_split);
			}
			$this->bindings[$field] = $value;
			$this->where_array[] = in_array($operation, ['IN', 'NOT IN']) ? "{$field} {$operation} (:{$field})" : "{$field} {$operation} :{$field}";
		}
	}

	/**
	 * Prepares data for INSERT query
	 *
	 * @param $data
	 */
	public function insert_bindings($data) {
		$this->reset_internal_data();
		foreach ($data as $field => $value) {
			$this->bindings[$field] = $value;
			$this->sets[] = $field;
			$this->values[] = ":{$field}";
		}
	}

	/**
	 * Executes INSERT query and returns last inserted id
	 *
	 * @param array $data
	 * @return int
	 */
	public function insert($data = []) {
		$this->insert_bindings($data);
		if (empty($this->bindings)) return 0;

		$this->last_sql = "INSERT INTO \"{$this->table}\" (".implode(',', $this->sets).") VALUES (".implode(',', $this->values).");";
		$pdo_result = $this->query($this->last_sql)
			->bind_array()
			->execute();
		return $pdo_result ? $this->inserted_id() : 0;
	}

	/**
	 * Prepares data for UPDATE query
	 *
	 * @param $data
	 */
	public function update_bindings($data) {
		$this->reset_internal_data();
		foreach ($data as $field => $value) {
			$this->bindings["{$field}_where"] = $value;
			$this->sets[] = "{$field} = :{$field}_where";
		}
	}

	/**
	 * Executes update query and returns number of affected rows
	 *
	 * @param array $data
	 * @param array $where
	 * @param string $end
	 * @return int
	 */
	public function update($data = [], $where = [], $end = ';') {
		$this->update_bindings($data);
		$this->where($where);

		if (empty($this->bindings) || empty($this->where_array)) return 0;

		$this->last_sql = "UPDATE \"{$this->table}\" SET ".implode(',', $this->sets)." WHERE ".implode(' AND ', $this->where_array).$end;
		$pdo_result = $this->query($this->last_sql)
			->bind_array()
			->execute();
		return $pdo_result ? $this->affected_rows() : 0;
	}

	/**
	 * Executes DELETE query and returns number of affected rows
	 *
	 * @param $where
	 * @return int
	 */
	public function delete($where) {
		$this->reset_internal_data();
		$this->where($where);

		$this->last_sql = "DELETE FROM \"{$this->table}\" WHERE ".implode(' AND ', $this->where_array);
		$pdo_result = $this->query($this->last_sql)
			->bind_array()
			->execute();
		return $pdo_result ? $this->affected_rows() : 0;
	}

	/**
	 * Returns last prepared PDO query
	 *
	 * @return string
	 */
	public function last_query() {
		return $this->last_sql;
	}

	/**
	 * Resets internal data arrays used to build query
	 *
	 */
	public function reset_internal_data() {
		$this->bindings = [];
		$this->where_array = [];
		$this->sets = [];
		$this->values = [];
	}

	/**
	 * Executes query
	 *
	 * @param string $query
	 * @return $this
	 */
	public function query($query = '') {
		if (!$query) $query = $this->last_sql;
		$this->statement = Database::connection()->prepare($query);
		return $this;
	}

	/**
	 * Binds data to prepared PDO query
	 *
	 * @param array $data
	 * @return $this
	 */
	public function bind_array($data = []) {
		if (empty($data)) $data = $this->bindings;
		foreach ($data as $key => $value) {
			$this->bind($key, $value);
		}
		return $this;
	}

	/**
	 * Binds single value to query
	 *
	 * @param $key
	 * @param $value
	 * @param null $type
	 * @return $this
	 */
	public function bind($key, $value, $type = null) {
		if( is_null($type) ) {
			switch( true ) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->statement->bindValue($key, $value, $type);
		return $this;
	}

	/**
	 * Executes query using stored PDO statement
	 *
	 * @return mixed
	 */
	public function execute() {
		return $this->statement->execute();
	}

	/**
	 * Fetches result as multiple rows
	 *
	 * @return mixed
	 */
	public function result() {
		$this->execute();
		return $this->statement->fetchAll(PDO::FETCH_OBJ);
	}

	/**
	 * Fetches result as single row
	 *
	 * @return mixed
	 */
	public function result_row() {
		$this->execute();
		return $this->statement->fetch(PDO::FETCH_OBJ);
	}

	/**
	 * Returns number of affected rows in last query
	 *
	 * @return mixed
	 */
	public function affected_rows() {
		return $this->statement->rowCount();
	}

	/**
	 * Returns last inserted id
	 *
	 * @return mixed
	 */
	public function inserted_id() {
		return Database::connection()->lastInsertId();
	}

	/**
	 * Outputs PDO debug info
	 */
	public function debug() {
		$this->statement->debugDumpParams();
	}

	/**
	 * Returns current timestamp as (postgre/timestamp or mysql/datetime)
	 *
	 * @return false|string
	 */
	public function timestamp_now() {
		return date("Y-m-d H:i:s");
	}

}