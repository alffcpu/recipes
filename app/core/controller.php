<?php
namespace Thesuper\Recipes\Core;

use Thesuper\Recipes\Models\User;

/**
 * Class Controller
 * @package Thesuper\Recipes\Core
 */
class Controller {

	/**
	 * Curent session_id (token)
	 * @var bool|null
	 */
	protected $session_id = false;

	/**
	 * Current user information
	 * @var bool
	 */
	protected $user = false;

	/**
	 * Controller constructor. Initializes session.
	 */
	public function __construct() {
		if ($session_id = Request::value('token')) {
			Session::set_id($session_id);
		} else {
			Session::get_id();
		}
		$this->session_id = $session_id;
	}

	/**
	 * Validates if controller and method has public access (accessible without valid token)
	 *
	 * @param $class_name
	 * @param $method_name
	 * @return bool
	 */
	public function access($class_name, $method_name) {
		$public = Config::get('public_access');
		if (isset($public[$class_name]) && in_array($method_name, $public[$class_name])) {
			return true;
		} else if ($this->session_id && Session::get('authorized') === true && $authorized_user_id = intval(Session::get('authorized_user_id'))) {
			$user_model = new User();
			if ( (!$this->user = $user_model->logged_user($this->session_id)) || ($this->user->id != $authorized_user_id) ) {
				$this->trigger_error('access_error', 'Unable to restore logged in user. Please, log out and log in again.');
			}
			return true;
		}
		$this->trigger_error('access_denied', 'Token expired or invalid');
		exit();
	}

	/**
	 * Triggers user error
	 *
	 * @param $error_mnemonic
	 * @param $error_description
	 */
	public function trigger_error($error_mnemonic, $error_description) {
		trigger_error(json_encode([$error_mnemonic, $error_description]), E_USER_ERROR);
	}

}
