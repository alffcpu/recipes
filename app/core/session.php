<?php
namespace Thesuper\Recipes\Core;

/**
 * Class Session
 * @package Thesuper\Recipes\Core
 */
class Session {

	/**
	 * Sets session id
	 *
	 * @param $session_id
	 * @return bool
	 */
	public static function set_id($session_id) {
		session_id($session_id);
		return session_start();
	}

	/**
	 * Returns session id and starts session if needed
	 *
	 * @return string
	 */
	public static function get_id() {
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		return session_id();
	}

	/**
	 * Sets multiple values in session (from array)
	 *
	 * @param $array
	 */
	public static function set_array($array) {
		foreach ($array as $key => $value) {
			self::set($key, $value);
		}
	}

	/**
	 * Sets single value in array
	 *
	 * @param $key
	 * @param $value
	 */
	public static function set($key, $value) {
		$_SESSION[$key] = $value;
	}

	/**
	 * Returns single value from session
	 *
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	public static function get($key, $default = null) {
		return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
	}

	/**
	 * Destroys current session
	 *
	 * @return bool
	 */
	public static function destroy() {
		return session_destroy();
	}

	/**
	 * Closes current session
	 *
	 */
	public static function close() {
		return session_write_close();
	}

}