<?php
namespace Thesuper\Recipes\Core;

/**
 * Class Helpers
 * @package Thesuper\Recipes\Core
 */
class Helpers {

	/**
	 * Converts datetime fields in result object to ISO with tiomezone
	 *
	 * @param $row
	 * @param $keys
	 */
	public static function row_atom_date($row, $keys) {
		if (!is_array($keys)) $keys = [$keys];
		foreach ($keys as $key) {
			if (isset($row->{$key}) && $row->{$key} !== null) {
				$date = new \DateTime($row->{$key});
				$row->{$key} = $date->format(\DateTime::ATOM);
			}
		}
	}
}