<?php
namespace Thesuper\Recipes\Core;

/**
 * Class Config
 * @package Thesuper\Recipes\Core
 */
class Config{

	protected static $data = [];

	/**
	 * Initializes application configuration
	 *
	 * @param array $config
	 */
	public static function init($config = []) {
		$config['document_root'] = rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/';
		$config['app_root'] = "{$config['document_root']}app/";
		$config['controllers_path'] = "{$config['app_root']}controllers/";
		self::$data = $config;
	}

	/**
	 * Gets value from configuration
	 *
	 * @param $key
	 * @param null $default
	 * @return mixed|null
	 */
	public static function get($key, $default = null) {
		return isset(self::$data[$key]) ? self::$data[$key] : $default;
	}

}
