<?php
namespace Thesuper\Recipes\Core;

use \PDO;
use \PDOException;

/**
 * Class Database
 * @package Thesuper\Recipes\Core
 */
class Database {

	/**
	 * Stores current connection to database
	 * @var
	 */
	protected static $connection;

	/**
	 * Tries to connect to database with provided parametrs
	 *
	 * @param $host
	 * @param $user
	 * @param $pass
	 * @param $dbname
	 */
	public static function connect($host, $user, $pass, $dbname) {
		try {
			self::$connection = new PDO(
				"pgsql:host={$host};dbname={$dbname}",
				$user,
				$pass,
				[PDO::ATTR_PERSISTENT => true]
			);
		} catch (PDOException $e) {
			Response::error('Database connection error: '.$e->getMessage());
			Response::set_error_code(500);
			exit();
		}
	}

	/**
	 * Returnes current database connection
	 *
	 * @return mixed
	 */
	public static function connection() {
		return self::$connection;
	}

}