<?php
namespace Thesuper\Recipes\Core;

/**
 * Class Response
 * @package Thesuper\Recipes\Core
 */
class Response {

	/**
	 * Error code for error response
	 * @var int
	 */
	private static $error_code = 400;

	/**
	 * Base response structure (empty fileds/arrays will be removed)
	 * @var array
	 */
	private static $response = [
		'error' => [],
		'error_description' => [],
		'message' => [],
		'output' => []
	];

	/**
	 * Set http header according to code
	 *
	 * @param int $code
	 */
	public static function set_header($code = 200) {
		$status_codes = array (
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			422 => 'Unprocessable Entity',
			423 => 'Locked',
			424 => 'Failed Dependency',
			426 => 'Upgrade Required',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			506 => 'Variant Also Negotiates',
			507 => 'Insufficient Storage',
			509 => 'Bandwidth Limit Exceeded',
			510 => 'Not Extended'
		);

		if (!array_key_exists($code, $status_codes)) {
			$code = self::$error_code;
		}
		$status_string = $code . ' ' . $status_codes[$code];
		header("{$_SERVER['SERVER_PROTOCOL']} {$status_string}", true, $code);
	}

	/**
	 * Adds application/json header
	 */
	public static function json_header() {
		header('Content-Type: application/json');
	}

	/**
	 * Output unsuccessfull result with some data
	 *
	 * @param $data
	 * @param bool $error_code
	 */
	public static function fail($data, $error_code = false) {
		if (!$error_code) {
			$error_code = self::$error_code;
		}
		self::set_header($error_code);
		self::json_header();
		$data['status'] = 'fail';
		self::make($data);
	}

	/**
	 * Output successfull result with some data
	 *
	 * @param $data
	 */
	public static function success($data) {
		self::set_header(200);
		self::json_header();
		$data['status'] = 'success';
		self::make($data);
	}

	/**
	 * Adds multiple key/value pairs from array to final response
	 *
	 * @param $array
	 */
	public static function data_array($array) {
		foreach ($array as $key => $value) {
			self::data($key, $value);
		}
	}

	/**
	 * Adds single key/value pair to final response
	 *
	 * @param $key
	 * @param $value
	 */
	public static function data($key, $value) {
		self::$response[$key] = $value;
	}

	/**
	 * Adds error mnemonic to final response
	 * Mnemonics are human readable codes without spaces
	 *
	 * @param $string
	 * @param null $key
	 */
	public static function error_mnemonic($string, $key = null) {
		self::add_to_response('error', $string, $key);
	}

	/**
	 * Adds error description to final request
	 * Error description may describe error mnemonic in details.
	 *
	 * @param $string
	 * @param null $key
	 */
	public static function error($string, $key = null) {
		self::add_to_response('error_description', $string, $key);
	}

	/**
	 * Adds message to request
	 *
	 * @param $string
	 * @param null $key
	 */
	public static function message($string, $key = null) {
		self::add_to_response('message', $string, $key);
	}

	/**
	 * Adds output of script while execution (error notes, etc)
	 *
	 * @param $string
	 * @param null $key
	 */
	public static function output($string, $key = null) {
		self::add_to_response('output', $string, $key);
	}

	/**
	 * Adds to response some data by key (from base structure)
	 *
	 * @param $respnose_key
	 * @param $string
	 * @param null $data_key
	 */
	public static function add_to_response($respnose_key, $string, $data_key = null) {
		if ($data_key) {
			self::$response[$respnose_key][$data_key] = $string;
		} else {
			self::$response[$respnose_key][] = $string;
		}
	}

	/**
	 * Makes final response (encode data to json format)
	 *
	 * @param $data
	 */
	public static function make($data) {
		echo json_encode($data);
	}

	/**
	 * Outputs final response with all neccesary information adn headers
	 *
	 */
	public static function create() {
		$response = self::$response;
		$is_error = !empty($response['error']) || !empty($response['error_description']);
		self::set_header( $is_error ? self::$error_code : 200 );
		self::json_header();
		$response['status'] = $is_error ? 'fail' : 'success';
		self::make(array_filter($response));
	}

	/**
	 * Sets error code for "fail" response
	 *
	 * @param $code
	 */
	public static function set_error_code($code) {
		self::$error_code = $code;
	}

}