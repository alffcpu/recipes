<?php
namespace Thesuper\Recipes\Core;

register_shutdown_function(function() {

	$error = error_get_last();
	if ( $error["type"] == \E_ERROR ) {
		Response::error("Fatal exec. error: {$error["message"]}");
	}

	$output = trim(ob_get_contents());
	if ($output) {
		Response::output($output);
	}

	ob_end_clean();
	Response::create();
});

ob_start();
call_user_func(function() {
	/* @var $config */
	foreach (['config', '../config', 'response', 'controller', 'database'] as $class_name) {
		require(realpath(__DIR__ . "/{$class_name}.php"));
	}
	Config::init($config);
	$db = $config['db'];
	Database::connect($db['host'], $db['user'], $db['passowrd'], $db['basename']);
});

ini_set("display_errors", "off");
error_reporting(Config::get('error_level'));
mb_internal_encoding('utf-8');
date_default_timezone_set("UTC");

spl_autoload_register(function ($class_name) {

	$name_array = explode("\\", strtolower($class_name));
	if ($name_array[0] != "thesuper") return;

	$root = Config::get('app_root');

	$class_filename = str_ireplace(['controller', 'service'], '', array_pop($name_array));
	$folder = array_pop($name_array);

	$file_path = "{$root}{$folder}/{$class_filename}.php";
	if (is_dir($root.$folder) && file_exists($file_path)) {
		require($file_path);
	} else {
		Response::set_error_code(503);
		Response::error("Class `{$class_name}` not found in: {$file_path}");
		exit();
	}
});

set_error_handler(function($error_number, $error_string, $error_file, $error_line){
	$error_description = $error_string;
	if ($error_number === E_USER_ERROR) {
		$user_error = json_decode($error_string, true);
		if (is_array($user_error) && count($user_error) == 2) {
			Response::error_mnemonic($user_error[0]);
			Response::error($user_error[1]);
			return false;
		}
	} else {
		$error_description = "Exec. error {$error_number}: {$error_string} [{$error_file} :: {$error_line}]";
	}
	Response::error($error_description);
	return false;
});

set_exception_handler(function($exception){
	Response::error('Exec. unhandled exception: '.$exception->getMessage().' ['.$exception->getFile().' :: '.$exception->getLine().']');
	return false;
});

