<?php
namespace Thesuper\Recipes\Controllers;

use Thesuper\Recipes\Core\Config;
use Thesuper\Recipes\Core\Controller;
use Thesuper\Recipes\Core\Helpers;
use Thesuper\Recipes\Core\Response;
use Thesuper\Recipes\Models\Image;
use Thesuper\Recipes\Models\Recipe;
use Thesuper\Recipes\Services\ValidationService;

/**
 * Class RecipeController
 * @package Thesuper\Recipes\Controllers
 */
class RecipeController extends Controller {

	/**
	 *  /recipe/create/ method
	 */
	public function create() {
		$validator = new ValidationService(['title', 'description']);
		$request = $validator->perform([
			'title' => 'prepare_string',
			'description' => 'trim'
		]);

		$recipe = new Recipe();
		$add_values = [
			'user_id' => $this->user->id,
			'title' => $request->title,
			'description' => $request->description
		];

		if ($recipe->select($add_values)) {
			$this->trigger_error('recipe_exits', 'There is atleast one exact the same recipe');
			return;
		}

		$add_values['date_added'] = $recipe->timestamp_now();
		$last_id = $recipe->insert($add_values);

		if ($last_id) {
			Response::data_array([
				'recipe_id' => $last_id
			]);
			return;
		}
		$this->trigger_error('recipe_create_error', 'Unable to add recipe due to interal error');
	}

	/**
	 *  /recipe/update/ method
	 */
	public function update() {
		$validator = new ValidationService(['title', 'description', 'recipe_id']);
		$request = $validator->perform([
			'title' => 'prepare_string',
			'description' => 'trim',
			'recipe_id' => 'prepare_int'
		]);

		$recipe = new Recipe();
		$update_values = [
			'title' => $request->title,
			'description' => $request->description
		];

		if (!$recipe->select(['id' => $request->recipe_id, 'user_id' => $this->user->id])) {
			$this->not_exists($request->recipe_id);
		}

		$update_values['date_updated'] = $recipe->timestamp_now();
		if (!$recipe->update($update_values, ['id' => $request->recipe_id])) {
			$this->trigger_error('recipe_update_error', 'Unable to update recipe due to interal error');
		}
	}

	/**
	 * Triggers "recipe_does_not_exits" error and exits
	 *
	 * @param $recipe_id
	 */
	private function not_exists($recipe_id) {
		$this->trigger_error('recipe_does_not_exits', 'There is no recipe with id '.$recipe_id);
		exit;
	}

	/**
	 * Fetches one recipe by request "recipe_id" and current user id
	 *
	 * @return mixed
	 */
	private function item_by_id() {
		$validator = new ValidationService(['recipe_id']);
		$request = $validator->perform([
			'recipe_id' => 'prepare_int'
		]);
		$recipe = new Recipe();
		$item = $recipe->by_user_id($request->recipe_id, $this->user->id);

		if (!$item) {
			$this->not_exists($request->recipe_id);
		}
		return $item;
	}

	/**
	 *  /recipe/delete/ method
	 */
	public function delete() {
		$item = $this->item_by_id();
		$recipe = new Recipe();
		if ( !$recipe->remove($item->id, $this->user->id) ) {
			$this->trigger_error('recipe_delete_error', 'Unable to delete recipe due to interal error');
		}
	}

	/**
	 *  /recipe/list/ method
	 */
	public function list() {
		$validator = new ValidationService(['limit', 'offset', 'order', 'direction']);
		$validator->prepare([
			'limit' => 'prepare_int',
			'offset' => 'prepare_int',
			'order' => [
				'trim',
				'strtolower',
				function($value) {
					return in_array($value, ['id', 'title', 'description', 'date_added', 'date_updated']) ? $value : '';
				}
			],
			'direction' => [
				'trim',
				'strtoupper',
				function($value) {
					return in_array($value, ['ASC', 'DESC']) ? $value : '';
				}
			]
		]);
		$validator->filter();

		$query_limit = Config::get('query_limit');
		$limit = $validator->value('limit', $query_limit);
		if ($limit > $query_limit) $limit = $query_limit;

		$recipe = new Recipe();
		$list = $recipe->list_by_user_id(
			$this->user->id,
			$limit,
			$validator->value('offset', 0),
			$validator->value('order', 'date_added'),
			$validator->value('direction', 'DESC')
		);
		Response::data('list', $this->prepare_list($list));
	}

	/**
	 *  /recipe/item/ method
	 */
	public function item() {
		$item = $this->item_by_id();
		Response::data('item', $this->prepare_list([$item])[0]);
	}

	/**
	 * Fetches images for list of recipes and converts date (ISO with timezone)
	 *
	 * @param $list
	 * @return array
	 */
	private function prepare_list($list) {
		$prepared_list = [];
		foreach ($list as $row) {
			Helpers::row_atom_date($row, ['date_added', 'date_updated']);
			$prepared_list[$row->id] = $row;
		}
		unset($list);
		$is_slash = DIRECTORY_SEPARATOR != '/';
		$uplod_url = Config::get('upload_url');
		if (!empty($prepared_list)) {
			$image_model = new Image();
			$images = $image_model->list_for_recipes(array_keys($prepared_list));
			foreach ($images as $image) {
				$recipe_id = $image->recipe_id;
				unset($image->recipe_id);
				Helpers::row_atom_date($image, ['date_uploaded']);
				if ($is_slash) {
					$image->path = str_replace(DIRECTORY_SEPARATOR, '/', $image->path);
				}
				$image->path = $uplod_url.$image->path;
				$prepared_list[$recipe_id]->images[] = $image;
			}
		}
		return array_values($prepared_list);
	}

}
