<?php
namespace Thesuper\Recipes\Controllers;

use Thesuper\Recipes\Core\Controller;
use Thesuper\Recipes\Core\Response;
use Thesuper\Recipes\Core\Session;
use Thesuper\Recipes\Services\ValidationService;
use Thesuper\Recipes\Models\User;
use Thesuper\Recipes\Core\Config;

/**
 * Class UserController
 * @package Thesuper\Recipes\Controllers
 */
class UserController extends Controller {

	/**
	 * /user/authenticate/ method
	 */
	public function authenticate() {

		$validator = new ValidationService(['login', 'password']);
		$request = $validator->perform([
			'login' => 'prepare_string',
			'password' => 'trim'
		]);

		$user_model = new User();
		if ($user_model->log_in($request->login, $request->password)) {
			$user_id = Session::get('authorized_user_id');
			$session_id = Session::get_id();
			Response::data_array([
				'user_id' => $user_id,
				'token' => $session_id
			]);
			$user_model->update(['last_visit' => $user_model->timestamp_now(), 'session_id' => $session_id], ['id' => $user_id]);
			return;
		}
		$this->trigger_error('wrong_login_password', "Incorrect login or password");
		return;
	}

	/**
	 * /user/register/ method
	 */
	public function register() {

		$validator = new ValidationService(['login', 'password', 'name']);
		$request = $validator->perform([
			'login' => 'prepare_string',
			'password' => 'prepare_string',
			'name' => 'prepare_string'
		]);

		if (strlen($request->password) < 6) {
			$this->trigger_error('short_password', "Password is too short");
		}

		$user_model = new User();

		$user_exists = $user_model->select([
			'login' => $request->login
		]);
		if ($user_exists) {
			$this->trigger_error('user_exists', "User `{$request->login}` already registered");
		}

		$now = $user_model->timestamp_now();
		$session_id = Session::get_id();
		$last_id = $user_model->insert([
			'login' => $request->login,
			'password' => password_hash($request->password, Config::get('password_algo')),
			'session_id' => $session_id,
			'last_visit' => $now,
			'registered' => $now,
			'name' => $request->name
		]);

		$user_model->authorize($last_id);
		Response::data_array([
			'user_id' => $last_id,
			'token' => $session_id
		]);
	}

	/**
	 * /user/info/ method
	 */
	public function info() {
		$info = (array)$this->user;
		unset($info['password']);
		Response::data_array($info);
	}

	/**
	 * /user/logout/ method
	 */
	public function logout() {

		$user_model = new User();
		$user_model->unlink_session($this->session_id, 0);
		Session::destroy();
	}

}
