<?php
namespace Thesuper\Recipes\Controllers;

use Thesuper\Recipes\Core\Controller;
use Thesuper\Recipes\Core\Request;
use Thesuper\Recipes\Core\Response;
use Thesuper\Recipes\Models\Recipe;
use Thesuper\Recipes\Services\FileService;
use Thesuper\Recipes\Services\ValidationService;

/**
 * Class ImageController
 * @package Thesuper\Recipes\Controllers
 */
class ImageController extends Controller {

	/**
	 *  /image/upload/ method
	 */
	public function upload() {

		$validator = new ValidationService(['recipe_id']);
		$request = $validator->perform([
			'recipe_id' => 'prepare_int'
		]);

		$recipe = new Recipe();
		$item = $recipe->by_user_id($request->recipe_id, $this->user->id);

		if (!$item) {
			$this->trigger_error('recipe_does_not_exits', 'There is no recipe with id '.$request->recipe_id);
			return;
		}

		$error_mnemonic = 'no_images_were_uploaded';
		$error_description = 'None of images were uploaded successuflly';

		$files = new FileService();
		if (!empty($_FILES) && $upload_result = $files->upload_multipart($_FILES, $this->user->id, $request->recipe_id)) {
			Response::data_array($upload_result);
			if (empty($upload_result['success'])) {
				$this->trigger_error($error_mnemonic, $error_description);
			}
			return;
		} elseif (
			Request::is_json() &&
			Request::is_set('image_name') &&
			Request::is_set('image_data')
		) {
			try {
				$files->upload_json($this->user->id, $request->recipe_id);
				Response::data('success', Request::value('image_name'));
			} catch (\Exception $e) {
				$last_error = $e->getMessage();
				$this->trigger_error($error_mnemonic, "{$error_description} ({$last_error})");
			}
			return;
		}
		$this->trigger_error('no_images_found', 'Nither multipart/form-data file nor json image was found');
	}

	/**
	 *  /image/delete/ method
	 */
	public function delete() {

		$file_id = Request::value('image_id');
		if (!is_array($file_id)) {
			$file_id = [$file_id];
		}
		foreach ($file_id as $key => &$file_id_value) {
			if (!$file_id_value = intval($file_id_value)) {
				unset($file_id[$key]);
			}
		}
		if (empty($file_id)) {
			$this->trigger_error('no_image_ids', 'There were no correct image ids to delete');
		}

		$files = new FileService();
		$remove_result = $files->remove($file_id, $this->user->id);
		Response::data_array($remove_result);
		if (empty($remove_result['success'])) {
			$this->trigger_error('no_images_were_deleted', 'None of images were deleted successuflly');
		}
	}


}
