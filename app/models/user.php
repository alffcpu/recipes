<?php
namespace Thesuper\Recipes\Models;

use Thesuper\Recipes\Core\Model;
use Thesuper\Recipes\Core\Session;

/**
 * Class User
 * @package Thesuper\Recipes\Models
 */
class User extends Model {

	/**
	 * Database table name
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Authorizes user with given id and links current session to him)
	 *
	 * @param $user_id
	 * @return bool
	 */
	public function authorize($user_id) {
		$user = $this->id($user_id);
		if ($user) {
			$session_id = Session::get_id();
			Session::set_array([
				'authorized' => true,
				'authorized_user_id' => $user->id
			]);
			$this->unlink_session($session_id, $user->id);
			return true;
		}
		return false;
	}

	/**
	 * Updates session id and last_visit
	 *
	 * @param $user_id
	 * @param $session_id
	 * @param null $last_visit
	 * @return int
	 */
	public function update_session($user_id, $session_id, $last_visit = null) {

		$update_data = ['session_id' => $session_id];
		if ($last_visit) {
			$update_data[$last_visit] = $last_visit === true ? $this->timestamp_now() : $last_visit;
		}

		return $this->update($update_data, ['id' => $user_id]);
	}

	/**
	 * Tries to log in user by login and password
	 *
	 * @param $login
	 * @param $password
	 * @return bool
	 */
	public function log_in($login, $password) {
		$user = $this->select([
			'login' => $login
		]);
		if ($user && password_verify($password, $user->password)) {
			return $this->authorize($user->id);
		}
		return false;
	}

	/**
	 * Unlinks session id from all user but one
	 *
	 * @param $session_id
	 * @param $user_id
	 * @return int
	 */
	public function unlink_session($session_id, $user_id) {
		return $this->update(['session_id' => null], ['session_id' => $session_id, 'id !=' => $user_id]);
	}

	/**
	 * Fetches info about logged in user (by session information)
	 *
	 * @param $session_id
	 * @return mixed
	 */
	public function logged_user($session_id) {
		if ($user_id = Session::get('authorized_user_id')) {
			return $this->id($user_id);
		}
		return $this->select(['session_id' => $session_id]);
	}

}