<?php
namespace Thesuper\Recipes\Models;

use Thesuper\Recipes\Core\Model;

/**
 * Class Image
 * @package Thesuper\Recipes\Models
 */
class Image extends Model {

	/**
	 * Database table name
	 * @var string
	 */
	protected $table = 'entity_files';

	/**
	 * Returns list of images for several recipes
	 *
	 * @param $recipe_ids array
	 * @return mixed
	 */
	public function list_for_recipes($recipe_ids) {

		$this->reset_internal_data();
		$this->last_sql = "SELECT f.id, f.path, f.name, f.date_uploaded, f.size, ef.entity_id as recipe_id FROM \"{$this->table}\" ef LEFT JOIN \"files\" f ON ef.file_id = f.id WHERE ef.entity = 'recipe' AND  ef.entity_id IN (".implode(',', $recipe_ids).") ORDER BY date_uploaded DESC ;";
		return $this->query($this->last_sql)
			->result();

	}

}