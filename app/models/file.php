<?php
namespace Thesuper\Recipes\Models;

use Thesuper\Recipes\Core\Model;

/**
 * Class File
 * @package Thesuper\Recipes\Models
 */
class File extends Model {

	/**
	 * Database table name
	 * @var string
	 */
	protected $table = 'files';

}