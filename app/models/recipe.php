<?php
namespace Thesuper\Recipes\Models;

use Thesuper\Recipes\Core\Model;
use Thesuper\Recipes\Services\FileService;

/**
 * Class Recipe
 * @package Thesuper\Recipes\Models
 */
class Recipe extends Model {

	/**
	 * Database table name
	 * @var string
	 */
	protected $table = 'recipes';

	/**
	 * Return list of recipes by user id
	 *
	 * @param $user_id
	 * @param $limit
	 * @param $offset
	 * @param $order
	 * @param $direction
	 * @return mixed
	 */
	public function list_by_user_id($user_id, $limit, $offset, $order, $direction) {
		$query_portion = " ORDER BY {$order} {$direction}";
		if ($limit) {
			$query_portion .= " LIMIT {$limit}";
		}
		if ($offset) {
			$query_portion .= " OFFSET {$offset}";
		}
		$query_portion .= ";";
		return $this->get_list(['user_id' => $user_id], $query_portion);
	}

	/**
	 * Returns one recipe by user id
	 *
	 * @param $recipe_id
	 * @param $user_id
	 * @return mixed
	 */
	public function by_user_id($recipe_id, $user_id) {
		return $this->select([
			'id' => $recipe_id,
			'user_id' => $user_id
		]);
	}

	/**
	 * Removes recipe and its images
	 *
	 * @param $recipe_id
	 * @param $user_id
	 * @return bool
	 */
	public function remove($recipe_id, $user_id) {

		if ($this->delete(['id' => $recipe_id])) {
			$image_modal = new Image();
			$file_ids =[];
			foreach ($image_modal->list_for_recipes([$recipe_id]) as $image) {
				$file_ids[] = $image->id;
			}
			if (!empty($file_ids)) {
				$file_service = new FileService();
				$file_service->remove($file_ids, $user_id);
			}
			return true;
		}
		return false;
	}

}