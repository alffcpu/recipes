<?php
namespace Thesuper\Recipes\Services;

use Thesuper\Recipes\Core\Response;
use Thesuper\Recipes\Core\Request;

/**
 * Class ValidationService
 * @package Thesuper\Recipes\Services
 */
class ValidationService {

	/**
	 * Current validating data
	 * @var array
	 */
	private $data = [];

	/**
	 * ValidationService constructor. Initializes data
	 * @param $fields_list
	 */
	public function __construct($fields_list) {
		$this->data = Request::values($fields_list);
	}

	/**
	 * Simpliest validating (checks required size and current prepared size)
	 *
	 * @param array $required
	 * @throws \Exception
	 */
	public function validate($required = []) {
		if (!empty($required)) {
			$check = array_intersect_key(array_flip($required), $this->data);
			if (count($check) != count($required)) {
				throw new \Exception('One or more required fields are missing values');
			}
		}
	}

	/**
	 * Prepares current data by map of callbacks
	 *
	 * @param array $prepare
	 */
	public function prepare($prepare = []) {
		foreach ($prepare as $key => $functions) {
			if (isset($this->data[$key])) {
				if (!is_array($functions)) $functions = [$functions];
				$value =& $this->data[$key];
				foreach ($functions as $function) {
					if (is_callable($function) || function_exists($function)) {
						$value = $function($value);
					} elseif (method_exists($this, $function)) {
						$value = $this->{$function}($value);
					}
				}
			}
		}
	}

	/**
	 * Filters current data
	 */
	public function filter() {
		$this->data = array_filter($this->data);
	}

	/**
	 * Performes all validating functions on current data
	 *
	 * @param $prepare
	 * @param array $required
	 * @return object
	 */
	public function perform($prepare, $required = []) {
		$this->prepare($prepare);
		$this->filter();
		try {
			$this->validate(!empty($required) ? $required : array_keys($prepare));
			return (object)$this->data;
		} catch (\Exception $e) {
			Response::error($e->getMessage());
			Response::error_mnemonic('validation_error');
			exit();
		}
	}

	/**
	 * Returns current data
	 *
	 * @return array
	 */
	public function get_data() {
		return $this->data;
	}

	/**
	 * Return one value from current data
	 *
	 * @param $key
	 * @param null $default
	 * @return mixed|null
	 */
	public function value($key, $default = null) {
		return isset($this->data[$key]) ? $this->data[$key] : $default;
	}

	/**
	 * Converts some special chars
	 *
	 * @param $string
	 * @return mixed
	 */
	function specialchars($string) {
		$search =  array("&amp;", "&lt;", "&gt;", "&quot;", "&#34", "&#x22", "&#39", "&#x27", "<", ">", "\"");
		$replace = array("&amp;amp;", "&amp;lt;", "&amp;gt;", "&amp;quot;", "&amp;#34", "&amp;#x22", "&amp;#39", "&amp;#x27", "&lt;", "&gt;", "&quot;");
		return str_replace($search, $replace, $string);
	}

	/**
	 * Simple string prepare callback
	 *
	 * @param $string
	 * @return mixed
	 */
	private function prepare_string($string) {
		return $this->specialchars(trim($string));
	}

	/**
	 * Simple integer prepare callback
	 *
	 * @param $string
	 * @return int
	 */
	private function prepare_int($string) {
		return intval(trim($string));
	}

	/**
	 * Simple float prepare callback
	 *
	 * @param $string
	 * @return float
	 */
	private function prepare_float($string) {
		return floatval(trim($string));
	}

}