<?php
namespace Thesuper\Recipes\Services;

use Thesuper\Recipes\Core\Config;
use Thesuper\Recipes\Core\Request;
use Thesuper\Recipes\Models\Image;
use Thesuper\Recipes\Models\File;

/**
 * Class FileService
 * @package Thesuper\Recipes\Services
 */
class FileService {

	/**
	 * Uploads files from form-data/multipart format
	 *
	 * @param $files
	 * @param $user_id
	 * @param $entity_id
	 * @param string $entity
	 * @return array|bool
	 */
	public function upload_multipart($files, $user_id, $entity_id, $entity = 'recipe') {

		foreach ($files as $key => $file) {
			if ($file['error']) {
				unset($files[$key]);
			}
		}

		if (empty($files)) return false;

		$image_model = new Image();
		$file_model = new File();

		$result = [
			'success' => [],
			'fail' => []
		];
		$path = $this->files_upload_path($user_id);
		$this->create_directory($path);
		$document_root = Config::get('document_root');
		foreach ($files as $file) {

			$original_name = $file['name'];
			$temporary_name = $file['tmp_name'];
			$ext = pathinfo($original_name, PATHINFO_EXTENSION);
			if (!$this->check_extension($ext)) {
				$result['fail'][] = [$original_name, 'File type not allowed'];
				continue;
			}

			$new_name = md5(microtime().$original_name);
			$subdirectory = $path.substr($new_name, -3);
			$this->create_directory($subdirectory);
			$new_path = $subdirectory.DIRECTORY_SEPARATOR."{$new_name}.{$ext}";

			if (
				is_uploaded_file($temporary_name) &&
				is_readable($temporary_name) &&
				move_uploaded_file($temporary_name, $new_path)
			) {
				$file_id = $file_model->insert([
					'name' => $original_name,
					'size' => $file['size'],
					'path' => str_replace($document_root, '', $new_path),
					'user_id' => $user_id,
					'date_uploaded' => $file_model->timestamp_now()
				]);
				if ($file_id) {
					$image_id = $image_model->insert([
						'entity_id' => $entity_id,
						'entity' => $entity,
						'file_id' => $file_id
					]);
					if ($image_id) {
						$result['success'][] = $original_name;
						continue;
					}
				}
			}
			$result['fail'][] = [$original_name, 'Upload error'];
		}

		return $result;
	}

	/**
	 * Verifies extension according to given array of allowed extensions
	 *
	 * @param $ext
	 * @param null $allowed
	 * @return bool
	 */
	public function check_extension($ext, $allowed = null) {
		if (!$allowed || !is_array($allowed)) $allowed = Config::get('allowed_extensions');
		return in_array(strtolower($ext), $allowed);
	}

	/**
	 * Returns upload path according to user id
	 *
	 * @param $user_id
	 * @return string
	 */
	public function files_upload_path($user_id) {
		$ds = \DIRECTORY_SEPARATOR;
		$root = Config::get('document_root');
		$uploads = ltrim(Config::get('upload_dir'), $ds);
		return "{$root}{$uploads}{$user_id}{$ds}";
	}

	/**
	 * Creates directory if needed (recursively)
	 *
	 * @param $path
	 * @return bool
	 */
	public function create_directory($path) {
		if (!is_dir($path)) {
			return mkdir($path, Config::get('upload_dir_rights'), TRUE);
		}
		return true;
	}

	/**
	 * Uploads single file from json request
	 *
	 * @param $user_id
	 * @param $entity_id
	 * @param string $entity
	 * @return bool
	 * @throws \Exception
	 */
	public function upload_json($user_id, $entity_id, $entity = 'recipe') {

		$file_path = $this->files_upload_path($user_id);

		$image_model = new Image();
		$file_model = new File();

		$original_name = Request::value('image_name');
		$document_root = Config::get('document_root');

		$ext = pathinfo($original_name, PATHINFO_EXTENSION);
		if (!$this->check_extension($ext)) {
			throw new \Exception('File type not allowed');
		}

		$new_name = md5(microtime().$original_name);
		$file_path = $file_path.substr($new_name, -3);
		$this->create_directory($file_path);
		$file_full_path = $file_path.DIRECTORY_SEPARATOR."{$new_name}.{$ext}";
		if (file_put_contents($file_full_path, base64_decode(Request::value('image_data')))) {
			$file_size = filesize($file_full_path);
			$file_id = $file_model->insert([
				'name' => $original_name,
				'size' => $file_size ? $file_size : null,
				'path' => str_replace($document_root, '', $file_full_path),
				'user_id' => $user_id,
				'date_uploaded' => $file_model->timestamp_now()
			]);
			if ($file_id) {
				return (bool)$image_model->insert([
					'entity_id' => $entity_id,
					'entity' => $entity,
					'file_id' => $file_id
				]);
			}
		}
		throw new \Exception('Upload error');
	}

	/**
	 * Removes files from database and disk
	 *
	 * @param $file_ids
	 * @param $user_id
	 * @return array
	 */
	public function remove($file_ids, $user_id) {
		if (!is_array($file_ids)) $file_ids = [$file_ids];
		$file_model = new File();
		$document_root = Config::get('document_root');
		$result = [
			'success' => [],
			'fail' => []
		];
		foreach ($file_ids as $file_id) {
			$file = $file_model->select(['id' => $file_id, 'user_id' => $user_id]);
			if ($file) {

				$file_path = $document_root.ltrim($file->path, '/');
				unlink($file_path);

				$file_dir = pathinfo($file_path, PATHINFO_DIRNAME);
				if (count(glob($file_dir.'/*', GLOB_NOSORT)) === 0 ) {
					rmdir($file_dir);
				}

				$affected_rows = $file_model->delete(['id' => $file_id]);
				if ($affected_rows) {
					$result['success'][] = $file_id;
					continue;
				}
			}
			$result['fail'][] = $file_id;
		}
		return $result;
	}

}

















