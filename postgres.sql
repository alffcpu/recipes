-- Adminer 4.6.2 PostgreSQL dump

DROP TABLE IF EXISTS "files";
DROP SEQUENCE IF EXISTS files_id_seq;
CREATE SEQUENCE files_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."files" (
    "id" integer DEFAULT nextval('files_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "path" character varying(4096) NOT NULL,
    "date_uploaded" timestamp NOT NULL,
    "name" character varying(2048) NOT NULL,
    "size" bigint,
    CONSTRAINT "files_id" UNIQUE ("id"),
    CONSTRAINT "files_id2" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "entity_files";
DROP SEQUENCE IF EXISTS recipe_files_id_seq;
CREATE SEQUENCE recipe_files_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."entity_files" (
    "id" integer DEFAULT nextval('recipe_files_id_seq') NOT NULL,
    "entity_id" bigint NOT NULL,
    "file_id" bigint NOT NULL,
    "entity" character varying(128) NOT NULL,
    CONSTRAINT "entity_files_file_id" UNIQUE ("file_id"),
    CONSTRAINT "entity_files_id" PRIMARY KEY ("id"),
    CONSTRAINT "entity_files_id2" UNIQUE ("id"),
    CONSTRAINT "entity_files_file_id_fkey" FOREIGN KEY (file_id) REFERENCES files(id) ON DELETE CASCADE NOT DEFERRABLE
) WITH (oids = false);

CREATE INDEX "entity_files_entity" ON "public"."entity_files" USING btree ("entity");


DROP TABLE IF EXISTS "recipes";
DROP SEQUENCE IF EXISTS recipes_id_seq;
CREATE SEQUENCE recipes_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."recipes" (
    "id" integer DEFAULT nextval('recipes_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "title" character varying(2048) NOT NULL,
    "description" text NOT NULL,
    "date_added" timestamp NOT NULL,
    "date_updated" timestamp,
    CONSTRAINT "recipes_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "recipes_user_id" ON "public"."recipes" USING btree ("user_id");


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."users" (
    "id" integer DEFAULT nextval('users_id_seq') NOT NULL,
    "login" character varying(128) NOT NULL,
    "password" character varying(2048) NOT NULL,
    "session_id" character varying(256),
    "last_visit" timestamp,
    "name" character varying(128) NOT NULL,
    "registered" timestamp,
    CONSTRAINT "users_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "users_login" ON "public"."users" USING btree ("login");


-- 2018-05-02 21:56:08.089545+00
